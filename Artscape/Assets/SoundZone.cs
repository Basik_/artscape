﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundZone : MonoBehaviour {
	void OnTriggerEnter2D(Collider2D col)
    {
		Debug.Log("play sound");
		if(col.gameObject.tag == "Player")
		{
			GetComponent<AudioSource>().volume = 1;
		}
	}
	void OnTriggerExit2D(Collider2D col)
    {
		if(col.gameObject.tag == "Player")
		{
			GetComponent<AudioSource>().volume = 0;
		}
	}
	
}
