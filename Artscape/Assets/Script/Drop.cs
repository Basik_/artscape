﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drop : MonoBehaviour {
	[SerializeField]
	GameObject checkGravity;

	void Update(){
		if (checkGravity.GetComponent<NewtonSwapper>().gravity) {
			GetComponent<Collider2D>().enabled=true;
			GetComponent<Rigidbody2D> ().gravityScale = 1;
			gameObject.layer = 0;
		}
	}
	
	
}
