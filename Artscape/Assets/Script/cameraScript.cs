﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraScript : MonoBehaviour {
    [SerializeField]
    Camera cam1;
    [SerializeField]
    Camera cam2;
    [SerializeField]
    Camera cam3;

    [SerializeField]
    float screenTransistion1;
    [SerializeField]
    float screenTransistion2;
    [SerializeField]
    float screenTransistion3;
    public GameObject player;
    // Use this for initialization
    void Start () {
        cam1.gameObject.SetActive(true);
        cam2.gameObject.SetActive(false);
        cam3.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (player.transform.position.x < screenTransistion1)
        {
            cam1.gameObject.SetActive(true);
            cam2.gameObject.SetActive(false);
            cam3.gameObject.SetActive(false);

        }
        else if (player.transform.position.x >= screenTransistion1 && player.transform.position.x < screenTransistion2)
        {
            cam1.gameObject.SetActive(false);
            cam2.gameObject.SetActive(true);
            cam3.gameObject.SetActive(false);
        }
        else if (player.transform.position.x >= screenTransistion2)
        {
            cam1.gameObject.SetActive(false);
            cam2.gameObject.SetActive(false);
            cam3.gameObject.SetActive(true);
        }
    }
}
