﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceFloat : MonoBehaviour {
    [SerializeField]
    GameObject newton;
	[SerializeField]
	float gavityFall;
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            if (!newton.GetComponent<NewtonSwapper>().gravity)
            {
                col.gameObject.GetComponent<Rigidbody2D>().gravityScale = gavityFall;
				col.gameObject.GetComponent<Rigidbody2D> ().mass = 1f;
            }
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
			col.gameObject.GetComponent<Rigidbody2D> ().mass = 1000;
        }
    }
}
