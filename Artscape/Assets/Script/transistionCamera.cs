﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class transistionCamera : MonoBehaviour {
    [SerializeField]
    Camera cam1;
    [SerializeField]
    Camera cam2;

    [SerializeField]
    bool isFirstArea;
    void Start() {
        GetComponent<Renderer>().enabled = false;
        if (isFirstArea)
        {
            cam1.gameObject.SetActive(true);
            cam2.gameObject.SetActive(false);
        }
        else
        {
            //cam1.gameObject.SetActive(false);
            cam2.gameObject.SetActive(false);
        }
    }
    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.CompareTag("Player")) {
            Debug.Log("Hit");
            if (cam1.gameObject.activeSelf)
            {
                cam1.gameObject.SetActive(false);
                cam2.gameObject.SetActive(true);
            }
            else {
                cam1.gameObject.SetActive(true);
                cam2.gameObject.SetActive(false);
            }
        }
    }
}
