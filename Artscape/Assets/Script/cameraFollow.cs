﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour {
	[SerializeField]
	GameObject checkGame;
	[SerializeField]
	GameObject player;
	// Update is called once per frame
	void Update () {
		if(!checkGame.GetComponent<DoNotTouchSign>().gameActive)
		{
			transform.position = player.transform.position + new Vector3 (0,4,-11);
		}
	}
}
