﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ring : MonoBehaviour {
	[SerializeField]
	float lifeSpan;
	// Use this for initialization
	void Start () {
		Destroy(gameObject, lifeSpan);
	}

}
