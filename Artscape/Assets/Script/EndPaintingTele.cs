﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPaintingTele : MonoBehaviour {
	[SerializeField]
	GameObject PlayerPos, EndPoint;
	void OnTriggerEnter2D (Collider2D col) {
		
		if (col.gameObject.CompareTag ("Player")){
			PlayerPos.transform.position = EndPoint.transform.position;

		}
	}
}
