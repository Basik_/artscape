﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextLevelCameraFollow : MonoBehaviour {
	[SerializeField]
	GameObject checkGravity;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(checkGravity.GetComponent<NewtonSwapper>().gravity){
			gameObject.layer = 14;
		}
	}
}
