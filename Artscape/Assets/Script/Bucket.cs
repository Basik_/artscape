﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bucket : MonoBehaviour {
	
	public GameObject player;
	public bool bucketDropCheck = false;
	public bool fillUp;
	public Sprite fill;
	void Update()
	{
		if (player.GetComponent<PlayerMovement>().pickedUp)
		{
			if (player.GetComponent<PlayerMovement>().bucketPickUp)
			{
				transform.position = player.transform.position + player.GetComponent<PlayerMovement>().holdPosition;
				GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
			}
		}
		else if (player.GetComponent<PlayerMovement>().pickedUp && player.GetComponent<PlayerMovement>().onInteractable && Input.GetMouseButtonDown(0)) {
			GetComponent<Collider2D>().enabled = true;
			player.GetComponent<PlayerMovement>().pickedUp = false;

		}
		if(fillUp){
			GetComponent<SpriteRenderer>().sprite = fill;
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		Debug.Log("hit");
		if (col.gameObject == player)
		{
			if (!player.GetComponent<PlayerMovement>().pickedUp)
			{
				Debug.Log("Collide");
				//GetComponent<Collider2D>().enabled = false;
				player.GetComponent<PlayerMovement>().pickedUp = true;
				player.GetComponent<PlayerMovement>().bucketPickUp = true;
			}
		}
	}
}
