﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class EndTrigger : MonoBehaviour {

	void Update () {
		if (gameObject.GetComponent<Trigger> ().active) {
		
			SceneManager.LoadScene ("End");
		}
	}

}
