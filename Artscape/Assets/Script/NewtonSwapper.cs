﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewtonSwapper : MonoBehaviour {
	
	[SerializeField]
	GameObject NewtonSleep, NewtonAwake, FlagUp, FlagDown,NewtonSnoreAudio;

    public bool gravity = false;

	void OnCollisionEnter2D (Collision2D col) {
		Debug.Log ("Newton Hit");
		if (col.gameObject.name == "AppleGO") {
			NewtonSleep.SetActive (false);
			NewtonAwake.SetActive (true);
			NewtonSnoreAudio.SetActive (false);
			//FlagUp.SetActive (false);
			//FlagDown.SetActive (true);
			gravity = true;
		}

		}
	//If a collision is detected disable the sleeping newton sprite and enable the awake newton sprite. 
}
