﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextPuzzleCamera : MonoBehaviour {
	[SerializeField]
	Camera MainCam;
	[SerializeField]
	Camera cam1;
	[SerializeField]
	GameObject tran;

	private bool move = false;

	// Update is called once per frame
	void Update () {
		if(move)
		{
			lerpCamera(cam1);
		}
		if (MainCam.transform.position == cam1.transform.position) 
		{
			move = false;
		}
	}
	void lerpCamera(Camera cam){
		MainCam.transform.position = Vector3.Lerp (MainCam.transform.position, cam.transform.position, 5.0f * Time.fixedDeltaTime);
		MainCam.orthographicSize = Mathf.Lerp (MainCam.orthographicSize, cam.orthographicSize, 5.0f * Time.fixedDeltaTime);
	}
	void OnTriggerEnter2D(Collider2D col) {
		if (col.gameObject.CompareTag ("Player")) {
			col.gameObject.GetComponent<PlayerMovement>().click = true;
			move = true;
			Destroy(tran);
		}
	}
}
