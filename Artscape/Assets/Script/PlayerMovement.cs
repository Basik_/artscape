﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    float speed;

	[SerializeField]
	GameObject ring;
	[SerializeField]
	GameObject exclamationmark;
	[SerializeField]
	GameObject warning1;
	[SerializeField]
	GameObject warning2;
	[SerializeField]
	GameObject beanstalk;
	[SerializeField]
	Animator charaAnimator;
	[SerializeField]
	GameObject canvasGame;

	[SerializeField]
	Animator animation;
	[SerializeField]
	Sprite normal;
	[SerializeField]
	Sprite holding;
	[SerializeField]
	SpriteRenderer character;
    public bool pickedUp;

    public bool applePickUp = false;
	public bool bucketPickUp = false;
    [SerializeField]
    public bool onInteractable = false;
	

	public Vector3 pos;
	
	Vector3 postemp;
	
	public bool active;
	public bool click;
	public bool endGameReady = false;
	public bool endGame = false;
	public Transform endGameLocation;
	[HideInInspector]
	public Vector3 holdPosition;
    // Use this for initialization
    void Start()
    {
		click = true;
        pos = transform.position;
		holdPosition = new Vector2 (0.1f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
		float step = speed * Time.deltaTime;
		if(!endGame){
			RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

			if (Input.GetMouseButtonDown(0))
			{
				if(click){
					if(active){
						GameObject touch;
						postemp = Input.mousePosition;
						postemp.z = 11;
						touch = Instantiate(ring,Camera.main.ScreenToWorldPoint(postemp),transform.rotation);
					}
					if (hit.collider != null)
					{
						Debug.Log(hit.collider.gameObject.name);
						if (hit.collider.gameObject.name == "DoNotTouch"){
							hit.collider.gameObject.GetComponent<DoNotTouchSign>().gameActive = true;
							hit.collider.gameObject.GetComponent<DoNotTouchSign>().warning += 1;
							if(hit.collider.gameObject.GetComponent<DoNotTouchSign>().warning == 1){
								GameObject warningInfo = Instantiate(warning1,Camera.main.ScreenToWorldPoint(postemp),transform.rotation);
							}
							else if(hit.collider.gameObject.GetComponent<DoNotTouchSign>().warning == 2){
								GameObject warningInfo = Instantiate(warning2,Camera.main.ScreenToWorldPoint(postemp),transform.rotation);
							}
						}
						else if (hit.collider.gameObject.name == "AppleGO")
						{
							hit.collider.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
							hit.collider.gameObject.GetComponent<CircleCollider2D>().radius = 0.3f;
						}
						else if (hit.collider.gameObject.name == "BucketGo")
						{
							hit.collider.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
							hit.collider.gameObject.GetComponent<CircleCollider2D>().radius = 0.3f;
						}
						else if (hit.collider.gameObject.name == "PlayerGO"){
							if(endGameReady){
								endGame = true;
								pos.x = endGameLocation.position.x;

								beanstalk.SetActive(true);	
								IEnumerator credit = endCredit (3);
								StartCoroutine (credit);
								//SceneManager.LoadScene("End");
							}
							
						}
						else if (hit.collider.gameObject.tag == "Interactable") {
							hit.collider.gameObject.GetComponent<Trigger> ().active = true;
							if (hit.collider.gameObject.name == "AppleDrop")
							{
								if (pickedUp)
								{
									if (applePickUp)
									{
										if (onInteractable)
										{
											pickedUp = false;
											applePickUp = false;
										}
									}
								}
							}
							
						}
						else {
							//GetMousePosition();
						}
					}
					else {
						GetMousePosition();
					}
				}
			}
		}

        transform.position = Vector3.MoveTowards(transform.position, new Vector3(pos.x, transform.position.y, transform.position.z), step);
		flipRenderer ();
		if(pickedUp){
			character.sprite = holding;
		}
		else{
			character.sprite = normal;
		}
		if(pickedUp){
			
			if(transform.position.x != pos.x)
			{
				GetComponentInChildren<Animator>().SetBool ("holding", true);
				GetComponentInChildren<Animator>().SetBool ("walking", false);
			}
			else{
				GetComponentInChildren<Animator>().SetBool ("holding", false);
				GetComponentInChildren<Animator>().SetBool ("walking", false);
			}
		}
		else{
			if(transform.position.x != pos.x)
			{
				GetComponentInChildren<Animator>().SetBool ("walking", true);
				GetComponentInChildren<Animator>().SetBool ("holding", false);
			}
			else{
				GetComponentInChildren<Animator>().SetBool ("walking", false);
				GetComponentInChildren<Animator>().SetBool ("holding", false);
			}
		}

		
	}

    void GetMousePosition() {
        pos = Input.mousePosition;
        pos.z = 0;
        pos = Camera.main.ScreenToWorldPoint(pos);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag =="Interactable") {
			Debug.Log("On Interactable");
            onInteractable = true;
        }
		if (col.gameObject.name == "BucketFill")
		{
			Debug.Log("On BucketFill");
			if (pickedUp)
			{
				if (bucketPickUp)
				{
					if (onInteractable)
					{
						Debug.Log("change value");
						GetComponent<BucketVariable>().bucket.GetComponent<Bucket>().fillUp = true;
					}
				}
			}
		}
    }
    void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag == "Interactable")
        {
			Debug.Log("Off Interactable");
            onInteractable = false;
        }
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Interactable") {
			Debug.Log("On Interactable");
            onInteractable = true;
        }
		if(bucketPickUp==true)
		{
			if(GetComponent<BucketVariable>().bucket.GetComponent<Bucket>().fillUp == true)
			{
				if(col.gameObject.name == "PourWater"){
					endGameReady = true;
					exclamationmark.SetActive(true);
				}
			}
		}
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag == "Interactable")
        {
			Debug.Log("Off Interactable");
            onInteractable = false;
        }
		if(bucketPickUp==true)
		{
			if(GetComponent<BucketVariable>().bucket.GetComponent<Bucket>().fillUp == true)
			{
				if(col.gameObject.name == "PourWater"){
					endGameReady = false;
					exclamationmark.SetActive(false);
				}
			}
		}
    }
	void flipRenderer(){
		if (transform.position.x > pos.x) {
			character.flipX = true;
			holdPosition = new Vector2 (-0.1f, 0);
		}
		else if (transform.position.x < pos.x){
			character.flipX = false;
			holdPosition = new Vector2 (0.1f, 0f);
		}
	}
	IEnumerator endCredit(float waitTime){
	

		yield return new WaitForSeconds (waitTime);
		canvasGame.SetActive (true);
		IEnumerator finish = reloadGame (5);
		StartCoroutine (finish);
	}	
	IEnumerator reloadGame(float waitTime){


		yield return new WaitForSeconds (waitTime);

		SceneManager.LoadScene("Level1");
	}
}
