﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoNotTouchSign : MonoBehaviour {
	[SerializeField]
	Camera MainCam;
	[SerializeField]
	Camera cam1;
	[SerializeField]
	GameObject MainCamGameObject;
	[SerializeField]
	float speed;
	[SerializeField]
	GameObject playerOutside;
	[SerializeField]
	GameObject playerInside;	
	[SerializeField]
	GameObject outsideWorld;
	
	public bool gameActive = false;
	private bool teleport = true;
	public float warning = 0;
	void FixedUpdate () {
		if(warning > 2){
			/*if(playerOutside.GetComponent<PlayerMovement>().onInteractable){
				teleport = true;
			}*/
			if(gameActive){
				if(teleport){
					MainCamGameObject.GetComponent<cameraFollow>().enabled = false;
					Destroy(playerOutside.GetComponent<Rigidbody2D>());
					Destroy(playerOutside.GetComponent<Collider2D>());
					if(MainCam.transform.position != cam1.transform.position){
						lerpCamera(cam1);
					}
					if(MainCam.transform.position == cam1.transform.position){
						gameActive = false;
						playerInside.transform.position = playerOutside.transform.position;
						playerInside.SetActive(true);
						playerInside.GetComponent<PlayerMovement>().active = true;
						Destroy(playerOutside);
						Destroy(outsideWorld);
					}
					Debug.Log("Lerping");
				}
			}
		}
	}
	
	void lerpCamera(Camera cam){
		MainCam.transform.position = Vector3.Lerp (MainCam.transform.position, cam.transform.position, 5.0f * Time.fixedDeltaTime);
		MainCam.orthographicSize = Mathf.Lerp (MainCam.orthographicSize, cam.orthographicSize, 5.0f * Time.fixedDeltaTime);

		playerOutside.transform.position = Vector3.Lerp (playerOutside.transform.position, playerInside.transform.position + new Vector3(-0.3f,0f,0f), 5.0f * Time.fixedDeltaTime);
		playerOutside.transform.localScale = Vector3.Lerp (playerOutside.transform.localScale, playerInside.transform.localScale, 5.0f * Time.fixedDeltaTime);
		playerOutside.transform.Rotate(new Vector3(0,0,speed * 5.0f * Time.fixedDeltaTime));
	}


}
