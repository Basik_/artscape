﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpCamera : MonoBehaviour {
	[SerializeField]
	Camera MainCam;
	[SerializeField]
	Camera cam1;
	[SerializeField]
	Camera cam2;
	[SerializeField]
	GameObject player;

	private bool move = false;
	private bool moveToCam1 = false;
	private bool moveToCam2 = false;
	void Start(){
		GetComponent<Renderer>().enabled = false;
	}
	void FixedUpdate () {
		if (move) {
			if (MainCam.transform.position == cam1.transform.position) {
				moveToCam2 = true;
				player.GetComponent<PlayerMovement>().click = false;
			} 
			else if (MainCam.transform.position == cam2.transform.position) {
				moveToCam1 = true;
				player.GetComponent<PlayerMovement>().click = false;
			}
			if (moveToCam1) {
				lerpCamera(cam1);
			}
			if (moveToCam2) {
				lerpCamera(cam2);
			}

			if (MainCam.transform.position == cam1.transform.position) {
				move = false;
				moveToCam1 = false;
				player.GetComponent<PlayerMovement>().click = true;
			} else if (MainCam.transform.position == cam2.transform.position) {
				move = false;
				moveToCam2 = false;
				player.GetComponent<PlayerMovement>().click = true;
			}
		}
	}
	void OnTriggerEnter2D(Collider2D col) {
		if (col.gameObject.CompareTag ("Player")) {
			Debug.Log ("Hit");
			if(player.transform.position.x < transform.position.x)
			{
				player.GetComponent<PlayerMovement>().pos.x = player.GetComponent<PlayerMovement>().pos.x + 0.2f;
			}
			else if (player.transform.position.x > transform.position.x) {
				player.GetComponent<PlayerMovement>().pos.x = player.GetComponent<PlayerMovement>().pos.x + -0.2f;
			}
			move = true;

		}
	}
	void lerpCamera(Camera cam){
		MainCam.transform.position = Vector3.Lerp (MainCam.transform.position, cam.transform.position, 10.0f * Time.deltaTime);
		MainCam.orthographicSize = Mathf.Lerp (MainCam.orthographicSize, cam.orthographicSize, 10.0f * Time.deltaTime);
	}
}
