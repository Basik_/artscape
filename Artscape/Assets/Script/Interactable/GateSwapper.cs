﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateSwapper : MonoBehaviour {

	[SerializeField]
	GameObject open,close;

	void Update(){
		if (gameObject.GetComponent<Trigger> ().active) {
			if (close.activeSelf) {
				gameObject.GetComponent<BoxCollider2D> ().enabled = false;
				open.SetActive(true);
				close.SetActive(false);
				gameObject.GetComponent<Trigger> ().active = false;
			} else {
				gameObject.GetComponent<BoxCollider2D> ().enabled = true;
				open.SetActive(false);
				close.SetActive(true);
				gameObject.GetComponent<Trigger> ().active = false;
			}
		}
	}
}
