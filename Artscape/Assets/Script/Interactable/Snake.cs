﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour {
	[SerializeField]
	Animator animator;

	void Update () {
		if (GetComponent<Trigger> ().active) {
			animator.SetTrigger("active");

			GetComponent<Trigger> ().active = false;
		}
	}
}
