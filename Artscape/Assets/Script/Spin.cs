﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour {
	[SerializeField]
	float speed;
	
	// Update is called once per frame
	void Update () {
		if(GetComponent<Trigger>().active){
			transform.Rotate(new Vector3(0,0,speed * Time.deltaTime));
			//GetComponent<Trigger>().active = false;
		}
	}
}
