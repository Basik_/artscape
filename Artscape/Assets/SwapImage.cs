﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapImage : MonoBehaviour {
	[SerializeField]
	GameObject flagUp,flagdown,gravityCheck;

	// Update is called once per frame
	void Update () {
		if(gravityCheck.GetComponent<NewtonSwapper>().gravity){
			flagUp.SetActive(false);
			flagdown.SetActive(true);
		}
	}
}
